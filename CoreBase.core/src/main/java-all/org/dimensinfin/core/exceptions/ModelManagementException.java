//  PROJECT:        plugin.gef.core
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package org.dimensinfin.core.exceptions;

// - CLASS IMPLEMENTATION ...................................................................................
public class ModelManagementException extends Exception {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger			logger						= Logger.getLogger("plugin.gef.core.exceptions");
	private static final long serialVersionUID = 3932341071765077771L;

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public ModelManagementException (String message) {
		super(message);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
}

// - UNUSED CODE ............................................................................................
