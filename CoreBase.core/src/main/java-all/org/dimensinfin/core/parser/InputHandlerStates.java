//  PROJECT:        net.sf.vorg.core
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package org.dimensinfin.core.parser;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public enum InputHandlerStates {
	INVALID, ERROR, LINKED, NOT_LINKED, READY;
}

// - UNUSED CODE ............................................................................................
