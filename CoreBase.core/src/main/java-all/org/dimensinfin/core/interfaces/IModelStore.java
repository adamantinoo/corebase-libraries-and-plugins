package org.dimensinfin.core.interfaces;

public interface IModelStore {
	// - P R O P E R T I E S - F I R E D
	public static final String MODEL_STRUCTURE_CHANGED = "IModelStore.MODEL_STRUCTURE_CHANGED";
	public static final String NODE_STATE_CHANGED = "IModelStore.NODE_STATE_CHANGED";

	//	public void addNode(AbstractGEFNode newNode);

}
