//	PROJECT:        corebase.model (CORE.M)
//	AUTHORS:        Adam Antinoo - adamantinoo.git@gmail.com
//	COPYRIGHT:      (c) 2013-2017 by Dimensinfin Industries, all rights reserved.
//	ENVIRONMENT:		Java 1.6.
//	DESCRIPTION:		Library that defines the model classes to implement the core for a GEF based
//									Model-View-Controller. Code is as neutral as possible and made to be reused
//									on all Java development projects.
//                  Added more generic code to develop other Model-View-Controller patterns.
package org.dimensinfin.gef.core;

// - IMPORT SECTION .........................................................................................
import java.util.Vector;

import org.dimensinfin.core.interfaces.INodeModel;

// - INTERFACE IMPLEMENTATION ...............................................................................
public interface IGEFNode extends INodeModel {
	public IGEFNode accessParent ();

	// - M E T H O D - S E C T I O N ..........................................................................
	public void addChild (IGEFNode child);

	public Vector<IGEFNode> getChildren ();

	public void setDirty (final boolean dirtyState);

	public void setParent (IGEFNode parent);
}
// - UNUSED CODE ............................................................................................
