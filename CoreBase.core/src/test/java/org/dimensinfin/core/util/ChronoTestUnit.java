//  PROJECT:     corebase.model (CORE.M)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2013-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Java 1.7.
//  DESCRIPTION: Library that defines the model classes to implement the core for a GEF based
//               Model-View-Controller. Code is as neutral as possible and made to be reused
//               on all Java development projects.
package org.dimensinfin.core.util;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// - CLASS IMPLEMENTATION ...................................................................................
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ChronoTestUnit extends Chrono {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger logger = LoggerFactory.getLogger("ChronoTestUnit");

	// - F I E L D - S E C T I O N ............................................................................
	private Chrono chronoTest = null;

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	@Test
	public void test01PrintElapsed100ms() {
		logger.info(">> [ChronoTestUnit.test01PrintElapsed100ms]");
		long milliseconds = TimeUnit.MILLISECONDS.toMillis(100);
		chronoTest = new Chrono().setMillis(milliseconds);
		Assert.assertEquals("-> Verifying generateTimeStringStandard output..."
				, "0.100"
				, chronoTest.generateTimeStringStandard(milliseconds));
//		Assert.assertEquals("-> Verifying generateTimeString output..."
//				, "0s"
//				, chronoTest.generateTimeString(milliseconds));
//		Assert.assertEquals("-> Verifying generateTimeStringMillis output..."
//				, "0s 100ms"
//				, chronoTest.generateTimeStringMillis(milliseconds));
		logger.info("<< [ChronoTestUnit.test01PrintElapsed100ms]");
	}

	@Test
	public void test02PrintElapsed5seconds() {
		logger.info(">> [ChronoTestUnit.test02PrintElapsed5seconds]");
		long milliseconds = TimeUnit.SECONDS.toMillis(5)
				+ TimeUnit.MILLISECONDS.toMillis(200);
		chronoTest = new Chrono().setMillis(milliseconds);
		Assert.assertEquals("-> Verifying generateTimeStringStandard output..."
				, "05.200"
				, chronoTest.generateTimeStringStandard(milliseconds));
//		Assert.assertEquals("-> Verifying generateTimeString output..."
//				, "5s"
//				, chronoTest.generateTimeString(milliseconds));
//		Assert.assertEquals("-> Verifying generateTimeStringMillis output..."
//				, "5s 200ms"
//				, chronoTest.generateTimeStringMillis(milliseconds));
		logger.info("<< [ChronoTestUnit.test02PrintElapsed5seconds]");
	}

	@Test
	public void test03PrintElapsed2minutes() {
		logger.info(">> [ChronoTestUnit.test03PrintElapsed2minutes]");
		long milliseconds = TimeUnit.MINUTES.toMillis(2)
				+ TimeUnit.SECONDS.toMillis(6)
				+ TimeUnit.MILLISECONDS.toMillis(300);
		chronoTest = new Chrono().setMillis(milliseconds);
		Assert.assertEquals("-> Verifying generateTimeStringStandard output..."
				, "02:06.300"
				, chronoTest.generateTimeStringStandard(milliseconds));
//		Assert.assertEquals("-> Verifying generateTimeString output..."
//				, "2m 06s"
//				, chronoTest.generateTimeString(milliseconds));
//		Assert.assertEquals("-> Verifying generateTimeStringMillis output..."
//				, "2m 6s 300ms"
//				, chronoTest.generateTimeStringMillis(milliseconds));
		logger.info("<< [ChronoTestUnit.test03PrintElapsed2minutes]");
	}

	@Test
	public void test04PrintDuration() {
		logger.info(">> [ChronoTestUnit.test04PrintDuration]");
//		long milliseconds = TimeUnit.MINUTES.toMillis(2)
//				+TimeUnit.SECONDS.toMillis(6)
//				+ TimeUnit.MILLISECONDS.toMillis(300);
//		chronoTest = new Chrono().setMillis(milliseconds);
//		Assert.assertEquals("-> Verifying generateTimeStringStandard output..."
//				, "2m 6s 300ms"
//				, chronoTest.generateDurationString(milliseconds));
		logger.info("<< [ChronoTestUnit.test04PrintDuration]");
	}

	@Test
	public void test05PrintCompleteDurations() {
		logger.info(">> [ChronoTestUnit.test04PrintDuration]");
		long milliseconds = 0;
		// Do the tst for only MILLISECONDS
		milliseconds = TimeUnit.MILLISECONDS.toMillis(100);
		chronoTest = new Chrono().setMillis(milliseconds);
		Assert.assertEquals("-> Verifying Print Duration 100..."
				, "0s"
				, chronoTest.generateDurationString(milliseconds, ChronoOptions.DEFAULT));
		Assert.assertEquals("-> Verifying Print Duration 100..."
				, "100ms"
				, chronoTest.generateDurationString(milliseconds, ChronoOptions.SHOWMILLIS));

		// Do the tst for only SECONDS
		milliseconds = TimeUnit.SECONDS.toMillis(5)
				+ TimeUnit.MILLISECONDS.toMillis(200);
		chronoTest = new Chrono().setMillis(milliseconds);
		Assert.assertEquals("-> Verifying Print Duration 5 secs 200..."
				, "5s"
				, chronoTest.generateDurationString(milliseconds, ChronoOptions.DEFAULT));
		Assert.assertEquals("-> Verifying Print Duration 100..."
				, "5s 200ms"
				, chronoTest.generateDurationString(milliseconds, ChronoOptions.SHOWMILLIS));

		// Do the tst for only SECONDS
		milliseconds = TimeUnit.MINUTES.toMillis(2)
				+ TimeUnit.SECONDS.toMillis(6)
				+ TimeUnit.MILLISECONDS.toMillis(300);
		chronoTest = new Chrono().setMillis(milliseconds);
		Assert.assertEquals("-> Verifying Print Duration 2 min 6 secs 300..."
				, "2m 6s"
				, chronoTest.generateDurationString(milliseconds, ChronoOptions.DEFAULT));
		Assert.assertEquals("-> Verifying Print Duration 100..."
				, "2m 6s 300ms"
				, chronoTest.generateDurationString(milliseconds, ChronoOptions.SHOWMILLIS));

		logger.info("<< [ChronoTestUnit.test04PrintDuration]");
	}
}

// - UNUSED CODE ............................................................................................
